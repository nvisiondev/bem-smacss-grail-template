(function($){
	'use strict';

	// Get the JSON grid configuration promise
	var config_json = $.getJSON('grid.json'),
		// Get the document ready promise
		document_ready = $(document).ready(),
		// Design Grid Controller
		grid_controller = {
			/**
			 * Apply method, the single method.
			 *
			 * Takes an element, generates a grid
			 * according to configuration,
			 * and applies it as the element's background.
			 *
			 */
			apply_grid: function( el ){
				// Check if a configuration is defined
				if( this.hasOwnProperty('config') ){

					/**
					 * Generate a canvas element,
					 * and a drawing context.
					 */

					// Retrieve the element's width
					var width = $( el ).width(),
						height = 1,
						// Parse the number of columns and gutter
						columns = parseInt( this.config.columns ),
						gutter = parseInt( this.config.gutter ),
						// Compute the column width
						column_width = width / columns,
						// Generate the canvas object
						$canvas = $('<canvas>');

					// Size the canvas
					$canvas.width( width );
					$canvas.height( height );

					// Set the intrinsic dimensions of the canvas
					$canvas.get(0).width = width;
					$canvas.get(0).height = height;

					var // Retrieve the drawing context
						context = $canvas.get(0).getContext('2d'),
						// Prepare an increment
						i = 0,
						// ..And a position indicator.
						pos = 0;

					// Fill the canvas with grey
					context.fillStyle = 'rgb(200,200,200)';
					context.fillRect (0, 0, width, height);

					// Set the drawing color to a pinkish hue
					context.fillStyle = 'rgb(200,180,200)';

					for( i = 0; i<columns; i++ ){
						pos = i*column_width + gutter;
						context.fillRect (pos, 0, column_width - gutter * 2, height);
					}

					//$('body').append( $canvas );
					el.style.backgroundImage = 'url(' + $canvas.get(0).toDataURL() + ')';
				}
				else{
					console.log( 'Canvas configuration is not set' );
				}


			}
		};

	// When all promises resolve
	$.when( config_json, document_ready ).then( function( result ){
		grid_controller.config = result[0];
		grid_controller.apply_grid( document.getElementsByClassName('page')[0] );
	});

	// Regenerate the grid on resize
	$(window).on('resize',function(){
		grid_controller.apply_grid( document.getElementsByClassName('page')[0] );
	});

	$(document).on('click', 'nav a', function(event){
		event.preventDefault();
		$( '#layout_stylesheet' ).attr( 'href', $(this).attr( 'href' ) );
		$.get( $(this).attr( 'href' ), function(){
			grid_controller.apply_grid( document.getElementsByClassName('page')[0] );
		});
	});




})(jQuery.noConflict());
